FROM node:14
WORKDIR /home/node/app
COPY . ./
RUN npm install

USER node
EXPOSE 51005
CMD [ "npm", "start" ]
